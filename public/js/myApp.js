function getDistricts(url, provinceCode) {
    return $.ajax({
        data: {
            province_code: provinceCode
        },
        url: url,
        type: 'GET'
    });
}

function getWards(url, districtCode) {
    return $.ajax({
        data: {
            district_code: districtCode
        },
        url: url,
        type: 'GET'
    });
}

function getSchools(url, provinceCode, districtCode, wardCode) {
    return $.ajax({
        data: {
            province_code: provinceCode,
            district_code: districtCode,
            ward_code: wardCode
        },
        url: url,
        type: 'GET'
    });
}

function getClasses(url, schoolId) {
    return $.ajax({
        data: {
            school_id: schoolId,
        },
        url: url,
        type: 'GET'
    });
}


function calculateMoney(customerMoney, paymentType, interestFee, interestFeeType, borrowDays,
                        feeByDay, valueMoneyValue, valueDay, percentMonth, percentWeek, valueWeek) {
    let interestValue = 0;
    console.log(interestFeeType + '=====' + valueMoneyValue);
    if (paymentType === feeByDay) {
        if (interestFeeType === valueMoneyValue) {
            interestValue = customerMoney / 1000000 * interestFee * borrowDays;
        } else if (interestFeeType === valueDay) {
            interestValue = interestFee * borrowDays;
            console.log('valueDay');
        }
    } else if (paymentType === percentMonth) {
        interestValue = (borrowDays / 30) * (interestFee / 100) * customerMoney;
    } else if (paymentType === percentWeek) {
        interestValue = (borrowDays / 7) * (interestFee / 100) * customerMoney;
    } else if (paymentType === valueWeek) {
        interestValue = (borrowDays / 7) * interestFee;
    }

    return customerMoney + interestValue;
}

Number.prototype.format = function (n, x) {
    var re = '(\\d)(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\.' : '$') + ')';
    return this.toFixed(Math.max(0, ~~n)).replace(new RegExp(re, 'g'), '$1,');
};

function getMessages(messages) {
    let content = '';
    if (Array.isArray(messages) && messages.length > 0) {
        messages.map(function (item) {
            content += "<li>" + item + "</li>";
        });
    }
    return content;
}

function drawFunnelChart(data, id) {
    var chart = new D3Funnel("#" + id);
    chart.draw(data, "basic");
}

function drawChartPie(chartId, title, data) {

    Highcharts.chart(chartId, {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        title: {
            text: title
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    }
                }
            }
        },
        series: data
    });
}

function drawChartLine(titleValue, subTitle, labelxAxis, labelyAxis, seriesValue, chartId) {
    var title = {
        text: titleValue
    };
    var subtitle = {
        text: subTitle
    };
    var xAxis = {
        categories: labelxAxis
    };
    var yAxis = {
        title: {
            text: labelyAxis
        },
        plotLines: [{
            value: 0,
            width: 1,
            color: '#808080'
        }]
    };
    var tooltip = {
        valueSuffix: ' data'
    };
    var legend = {
        layout: 'vertical',
        align: 'right',
        verticalAlign: 'middle',
        borderWidth: 0
    };
    var series = seriesValue;

    var json = {};
    json.title = title;
    json.subtitle = subtitle;
    json.xAxis = xAxis;
    json.yAxis = yAxis;
    json.tooltip = tooltip;
    json.legend = legend;
    json.series = series;

    $('#' + chartId).highcharts(json);
}

function drawColsChart(chartId, title, subtitle, labelCategories, yAxis, dataSeries) {
    Highcharts.chart(chartId, {
        chart: {
            type: 'column'
        },
        title: {
            text: title
        },
        subtitle: {
            text: subtitle
        },
        xAxis: {
            categories: labelCategories,
            crosshair: true
        },
        yAxis: {
            min: 0,
            title: {
                text: yAxis
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y:.1f} mm</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series: dataSeries
    });
}

function drawSingleColChart(chartId, title, subtitle, yAxis, serieName,  dataSeries) {
    Highcharts.chart(chartId, {
        chart: {
            type: 'column'
        },
        title: {
            text: title
        },
        subtitle: {
            text: subtitle
        },
        xAxis: {
            type: 'category',
            labels: {
                rotation: -45,
                style: {
                    fontSize: '13px',
                }
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: yAxis
            }
        },
        legend: {
            enabled: false
        },
        tooltip: {
            pointFormat: ' <b>{point.y:.1f} vnd</b>'
        },
        series: [{
            name: serieName,
            data:
                dataSeries
            ,
            dataLabels: {
                enabled: true,
                rotation: -90,
                color: '#FFFFFF',
                align: 'right',
                format: '{point.y:.1f}', // one decimal
                y: 10, // 10 pixels down from the top
                style: {
                    fontSize: '13px',
                }
            }
        }]
    });
}


