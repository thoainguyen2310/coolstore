<?php
return [
    'status' => [
        -1 => 'Cancel ',
        1 => 'Pending ',
        2 => 'Wait paid',
        3 => 'Paid Success'
    ],
    'status_str' => [
        'cancel' => -1,
        'pending' => 1,
        'wait_paid' => 2,
        'paid_success' => 3
    ],
    'tax_default' => 2
];