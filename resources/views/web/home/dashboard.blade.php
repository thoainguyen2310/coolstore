@extends('web.layouts.master')
@section('content')

    @if(!empty($categories))
        <div class="row ">
            @include('web.layouts.components.alert')
            <form method="post" action="{{ route('orders.create') }}">
                {{ csrf_field() }}
                <div class="col-md-12">
                    <button type="submit" class="btn btn-sm btn-primary pull-right">Order</button>

                </div>
                @foreach($categories as $category)
                    <div class="col-md-12 alert alert-info-custom ">
                        <h3 class="text-center">{{ $category->name }}</h3>
                        @if(!empty($category->products))

                            @foreach($category->products as $product)
                                <div class="col-md-3  alert-info-custom">
                                    <h4 class="text-center">{{ $product->name }}</h4>
                                    <h5 class="text-center">Price: <span class="text-green my_font_16">{{ $product->price }} $</span>
                                    </h5>
                                    <div class="form-group text-center">
                                        <input class="text-center" type="checkbox" name="select_product[]"
                                               value="{{ $product->id }}">
                                    </div>
                                </div>
                            @endforeach
                        @endif
                    </div>
                @endforeach
            </form>
        </div>
    @endif

@endsection

@section('js')
    <script>
        $('#li_dashboard').addClass('active');
    </script>

@endsection