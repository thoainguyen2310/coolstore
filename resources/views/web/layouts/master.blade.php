<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>@if(!empty($title)) {{ 'UPS | '.  @$title }} @else UPS  @endif</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta name="csrf-token" content="{{ csrf_token() }}"/>
    <!-- Bootstrap 3.3.7 -->

    <link rel="stylesheet" href="{{ asset('themes/admin/bower_components/bootstrap/dist/css/bootstrap.min.css') }}">
    <link rel="shortcut icon" href="{{ asset('images/logo3.png') }}">

    <link rel="stylesheet" href="{{ asset('themes/admin/bower_components/font-awesome/css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('themes/admin/bower_components/Ionicons/css/ionicons.min.css') }}">

    <link rel="stylesheet" href="{{ asset('themes/admin/dist/css/AdminLTE.min.css') }}">


    <link rel="stylesheet" href="{{ asset('themes/admin/dist/css/skins/_all-skins.min.css') }}">
    <link rel="stylesheet" href="{{ asset('themes/admin/bower_components/morris.js/morris.css') }}">

    <link rel="stylesheet" href="{{ asset('themes/admin/bower_components/jvectormap/jquery-jvectormap.css') }}">

    <link rel="stylesheet"
          href="{{ asset('themes/admin/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">

    <link rel="stylesheet"
          href="{{ asset('themes/admin/bower_components/bootstrap-daterangepicker/daterangepicker.css') }}">
    <link rel="stylesheet" href="{{ asset('themes/admin/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') }}">

    <link rel="stylesheet" href="{{ asset('themes/admin/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') }}">

    {{--<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>--}}
    {{--<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>--}}

    <link rel="stylesheet" href="{{ asset('themes/admin/bower_components/select2/dist/css/select2.min.css') }}">

    <link rel="stylesheet" href="{{ asset('themes/admin/plugins/iCheck/all.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/jconfirm/jquery-confirm.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/selectpicker/bootstrap-select.min.css') }}">

    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
    <link rel="stylesheet" href="{{ asset('css/myApp.css') }}?v31">
    <link rel="stylesheet" href="{{ asset('css/myElement.css') }}?v3">
    <link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">

    @yield('css')

    <style>
        .content {
            /*padding: 16px;*/

            min-height: 250px;
            padding: 8px;
            margin-right: auto;
             margin-left: 3px;
            padding-left: 15px;
            padding-right: 3px;
        }
        .header {
            /*padding: 10px 16px;*/
        }

        .sticky {
            position: fixed;
            top: 0;
            width: 100%;
        }

        .sticky + .content {
            padding-top: 102px;
        }

    </style>
</head>
<body class="hold-transition skin-blue sidebar-mini">
{{--@include('layouts.components.loading')--}}
<div class="wrapper">

@include('web.layouts.components.header')
<!-- Left side column. contains the logo and sidebar -->
    @include('web.layouts.components.aside')
    <div class="content-wrapper ">
        @include('web.layouts.components.content_header')
        <section class="content">
            @yield('content')

        </section>


    </div>
    <!-- /.content-wrapper -->
@include('web.layouts.components.footer')

<!-- Control Sidebar -->
    <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->

<script src="{{ asset('themes/admin/bower_components/jquery/dist/jquery.min.js') }}"></script>
<!-- jQuery UI 1.11.4 -->

<script src="{{ asset('themes/admin/bower_components/jquery-ui/jquery-ui.min.js') }}"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
    $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.7 -->

<script src="{{ asset('themes/admin/bower_components/bootstrap/dist/js/bootstrap.min.js') }}"></script>


<script src="{{ asset('themes/admin/bower_components/raphael/raphael.min.js') }}"></script>

<script src="{{ asset('themes/admin/bower_components/morris.js/morris.min.js') }}"></script>

<script src="{{ asset('themes/admin/bower_components/raphael/raphael.min.js') }}"></script>
<!-- Sparkline -->
<script src="{{ asset('themes/admin/bower_components/jquery-sparkline/dist/jquery.sparkline.min.js') }}"></script>
<!-- jvectormap -->
<script src="{{ asset('themes/admin/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js') }}"></script>
<script src="{{ asset('themes/admin/plugins/jvectormap/jquery-jvectormap-world-mill-en.js') }}"></script>
<!-- jQuery Knob Chart -->
<script src="{{ asset('themes/admin/bower_components/jquery-knob/dist/jquery.knob.min.js') }}"></script>
<!-- daterangepicker -->
<script src="{{ asset('themes/admin/bower_components/moment/min/moment.min.js') }}"></script>
<script src="{{ asset('themes/admin/bower_components/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
<!-- datepicker -->
<script src="{{ asset('themes/admin/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="{{ asset('themes/admin/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') }}"></script>
<!-- Slimscroll -->
<script src="{{ asset('themes/admin/bower_components/jquery-slimscroll/jquery.slimscroll.min.js') }}"></script>
<!-- FastClick -->
<script src="{{ asset('themes/admin/bower_components/fastclick/lib/fastclick.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ asset('themes/admin/dist/js/adminlte.min.js') }}"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
{{--<script src="{{ asset('themes/admin/dist/js/pages/dashboard.js') }}"></script>--}}
<!-- AdminLTE for demo purposes -->
<script src="{{ asset('themes/admin/dist/js/demo.js') }}"></script>


<script src="{{ asset('themes/admin/bower_components/select2/dist/js/select2.full.min.js') }}"></script>

<script src="{{ asset('themes/admin/plugins/iCheck/icheck.min.js') }}"></script>


<script src="{{ asset('plugins/inputmask/dist/jquery.inputmask.bundle.js') }}"></script>
<script src="{{ asset('plugins/inputmask/dist/inputmask/bindings/inputmask.binding.js') }}"></script>
<script src="{{ asset('plugins/validate/dist/jquery.validate.min.js') }}"></script>
<script src="{{ asset('plugins/selectpicker/bootstrap-select.min.js') }}"></script>
<script src="{{ asset('plugins/jconfirm/jquery-confirm.min.js') }}"></script>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
<script src="{{ asset('js/myApp.js'.'?v='.strtotime(date('Y-m-d'))) }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/d3/3.5.3/d3.min.js"></script>
<script src="{{ asset('plugins/funnel_chart/d3-funnel.js') }}"></script>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap.min.js"></script>

{{--    <script src="https://code.highcharts.com/modules/exporting.js"></script>--}}
{{--    <script src="https://code.highcharts.com/modules/export-data.js"></script>--}}
@include('web.layouts.components.loading')

<script>
    {{--var url = window.location;--}}

    {{--// for sidebar menu entirely but not cover treeview--}}
    {{--$('ul.sidebar-menu a').filter(function() {--}}
    {{--return this.href == url;--}}
    {{--}).parent().addClass('active');--}}

    {{--// for treeview--}}
    {{--$('ul.treeview-menu a').filter(function() {--}}
    {{--return this.href == url;--}}
    {{--}).parentsUntil(".sidebar-menu > .treeview-menu").addClass('active');--}}

    {{--var feeByDay = parseInt('{{ config('apps.contract.payment_type_str.fee_by_day') }}');--}}
    {{--var percentMonth = parseInt('{{ config('apps.contract.payment_type_str.fee_by_month_for_percent') }}');--}}
    {{--var percentWeek = parseInt('{{ config('apps.contract.payment_type_str.fee_by_week_for_percent') }}');--}}
    {{--var valueWeek = parseInt('{{ config('apps.contract.payment_type_str.fee_by_week_for_value') }}');--}}
    {{--var moneyValueMoney = parseInt('{{ config('apps.contract.interest_fee_type_str.value_money_value') }}');--}}
    {{--var valueDay = parseInt('{{ config('apps.contract.interest_fee_type_str.value_day') }}');--}}
    $('.datepicker').datepicker({
        format: 'dd-mm-yyyy',
        autoclose: true,
        todayHighlight: true
    });

    function changePaginatePerPage(el) {
        let perPage = $(el).val();
        if (perPage) {
            setCookie('per_page', perPage, 365);
            window.location.reload();
        }
    }

    function setCookie(cname, cvalue, exdays) {
        let d = new Date();
        d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
        let expires = "expires=" + d.toUTCString();
        document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
    }
</script>
@yield('js')
@yield('js2')
<script src="https://js.pusher.com/4.4/pusher.min.js"></script>
<script>
    Pusher.logToConsole = false;

    var pusher = new Pusher('{{ env('PUSHER_APP_KEY') }}', {
        cluster: 'ap1',
        encrypted: true
    });

    // Subscribe to the channel we specified in our Laravel Event
    var channel = pusher.subscribe('ScheduleCallUserNotify');

    channel.bind('schedule-call-user-notify', function (data) {
        console.log(data);
    });

    function showInfoLevel() {
        $('#info_user_level_modal').modal('show');
    }

    window.onscroll = function() {myFunction()};

    var header = document.getElementById("myHeader");
    var sticky = header.offsetTop;

    function myFunction() {
        if (window.pageYOffset > sticky) {
            header.classList.add("sticky");
        } else {
            header.classList.remove("sticky");
        }
    }


</script>

</body>
</html>
