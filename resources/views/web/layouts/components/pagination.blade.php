{{--@if ($paginator->hasPages())--}}
{{--<div class="row">--}}
    <span class="pull-left" style="margin-top: 10px">Hiển thị {{$paginator->firstItem()}} đến {{$paginator->lastItem()}} -  Tổng {{$paginator->total()}}</span>

    <div class="pagination pull-right" style="margin: 0">
        <select onchange="changePaginatePerPage(this)" class="form-control pagination_per_page_select" style="width: 70px; float: left; margin-top: 20px; margin-right: 5px;">
            @foreach(config('general.record_per_page') as $record)
                @if($record == @$paginator->perPage())
                    <option value="{{ $record }}" selected>{{ $record }}</option>
                @else
                    <option value="{{ $record }}">{{ $record }}</option>
                @endif
            @endforeach

        </select>
        <ul class="pagination" role="navigation">
            {{-- Previous Page Link --}}
            @if ($paginator->onFirstPage())
                <li class="disabled" aria-disabled="true" aria-label="@lang('pagination.previous')">
                    <span aria-hidden="true">&lsaquo;</span>
                </li>
            @else
                <li>
                    <a href="{{ $paginator->previousPageUrl() }}" rel="prev" aria-label="@lang('pagination.previous')">&lsaquo;</a>
                </li>
            @endif

            {{-- Pagination Elements --}}
            @foreach ($elements as $element)
                {{-- "Three Dots" Separator --}}
                @if (is_string($element))
                    <li class="disabled" aria-disabled="true"><span>{{ $element }}</span></li>
                @endif

                {{-- Array Of Links --}}
                @if (is_array($element))
                    @foreach ($element as $page => $url)
                        @if ($page == $paginator->currentPage())
                            <li class="active" aria-current="page"><span>{{ $page }}</span></li>
                        @else
                            <li><a href="{{ $url }}">{{ $page }}</a></li>
                        @endif
                    @endforeach
                @endif
            @endforeach

            {{-- Next Page Link --}}
            @if ($paginator->hasMorePages())
                <li>
                    <a href="{{ $paginator->nextPageUrl() }}" rel="next" aria-label="@lang('pagination.next')">&rsaquo;</a>
                </li>
            @else
                <li class="disabled" aria-disabled="true" aria-label="@lang('pagination.next')">
                    <span aria-hidden="true">&rsaquo;</span>
                </li>
            @endif
        </ul>
    </div>
{{--</div>--}}
{{--@endif--}}
