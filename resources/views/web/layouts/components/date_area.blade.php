<div class="col-md-12 no-padding">
    <div class="col-md-2">
        <label>Từ ngày</label>
        <input class="form-control datepicker" readonly id="start_at" value="{{ date('d-m-Y', strtotime("-7 days", strtotime(date('Y-m-d')))) }}">
    </div>
    <div class="col-md-2">
        <label>đến ngày</label>
        <input class="form-control datepicker"readonly id="end_at" value="{{ date('d-m-Y') }}">
    </div>
</div>
