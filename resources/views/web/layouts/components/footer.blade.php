<footer class="main-footer">
    <div class="pull-right hidden-xs">
        <b>Ha noi</b> {{ date('d-m-Y') }}
    </div>
    <strong>Copyright &copy;2019 <a href="https://adminlte.io">Nguyen Thoai</a>.</strong> All rights
    reserved.
</footer>