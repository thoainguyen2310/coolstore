<section class="content-header">

    <h1>
        @if(!empty($title)) {{ @$title }} @endif
        <small> @if(!empty($subTitle)) {{ @$subTitle }} @endif</small>
    </h1>
    <div class="btn-parent pull-right" style="position: absolute; top: 10px; right: 5px">
        @yield('btn-parent')
    </div>
</section>