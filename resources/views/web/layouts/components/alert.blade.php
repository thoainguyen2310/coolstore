@if(session()->has('error'))
    <div class="alert alert-danger">
        {{session('error')}}
    </div>
@endif

@if(session()->has('message'))
    <div class="alert alert-info">
        {{session('info_message')}}
    </div>
@endif

@if(session()->has('success'))
    <div class="alert alert-success">
        {{session('success')}}
    </div>
@endif

@if (!empty($errors) && $errors->any())
    <div class="alert alert-danger">
        <ul style="margin-left: -30px; margin-bottom: 0px;">
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
