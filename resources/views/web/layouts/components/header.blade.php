<header class="main-header header" id="myHeader">
    <!-- Logo -->
    <a href="{{ route('dashboard') }}" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini">Cool</span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg"><b>Store</b></span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>

        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                @if(!Auth::guard('web')->check())
                    <li class="dropdown messages-menu">
                        <a href="{{ route('login') }}" class="dropdown-toggle">
                            Login
                        </a>
                    </li>
                @else
                    <li class="dropdown user user-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <img src="{{ asset('themes/admin/dist/img/user2-160x160.jpg') }}" class="user-image"
                                 alt="User Image">
                            <span class="hidden-xs"> {{ Auth::guard('web')->user()->fullname }}</span>
                        </a>
                        <ul class="dropdown-menu">
                            <!-- User image -->

                            <!-- Menu Footer-->
                            <li class="user-footer">
                                <div class="pull-right">
                                    <a href="{{ route('logout') }}" class="btn btn-default btn-flat">Log out</a>
                                </div>
                            </li>
                        </ul>
                    </li>
                @endif

                <!-- Messages: style can be found in dropdown.less-->
                <!-- User Account: style can be found in dropdown.less -->

            </ul>
        </div>
    </nav>
</header>

