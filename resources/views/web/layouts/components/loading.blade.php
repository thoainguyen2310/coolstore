<style media="screen" rel="stylesheet">
    /* Loading Spinner */
    .spinner{margin:0;width:70px;height:18px;margin:-35px 0 0 -9px;position:absolute;top:50%;left:50%;text-align:center}.spinner > div{width:18px;height:18px;background-color: #f2821e;border-radius:100%;display:inline-block;-webkit-animation:bouncedelay 1.4s infinite ease-in-out;animation:bouncedelay 1.4s infinite ease-in-out;-webkit-animation-fill-mode:both;animation-fill-mode:both}  .spinner .bounce1{-webkit-animation-delay:-.32s;animation-delay:-.32s}  .spinner .bounce2{-webkit-animation-delay:-.16s;animation-delay:-.16s}  @-webkit-keyframes bouncedelay{0%,80%,100%{-webkit-transform:scale(0.0)} 40%{-webkit-transform:scale(1.0)}}  @keyframes bouncedelay{0%,80%,100%{transform:scale(0.0);-webkit-transform:scale(0.0)} 40%{transform:scale(1.0);-webkit-transform:scale(1.0)}}
</style>
<div id="page-loading">
    <div class="spinner">
        <div class="bounce1"></div>
        <div class="bounce2"></div>
        <div class="bounce3"></div>
    </div>
</div>
<div id="loading" style="display: none;">
    <div class="spinner">
        <div class="bounce1"></div>
        <div class="bounce2"></div>
        <div class="bounce3"></div>
    </div>
</div>

<script type="text/javascript">
    $(window).on('load', function () {
        setTimeout(function() {
            $('#page-loading').fadeOut( 400, "linear" );
        }, 300);
    });
    function showLoading(){
        $('#loading').fadeIn( 100, "linear" );
    }
    function hideLoading(){
        $('#loading').hide();
    }
    $(window).on('beforeunload', function () {
        showLoading();
    });
</script>
