@extends('web.layouts.master')
@section('content')
    <div class="row alert alert-info-custom">
        <div class="col-md-12">
            <table class="table table-bordered table-data">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Order Code</th>
                    <th>Products</th>
                    <th>Tax</th>
                    <th>Coupon</th>
                    <th>Paid total</th>
                </tr>
                </thead>
                <tbody>
                @if(!empty($orders))
                    @foreach($orders as $key => $order)
                        <tr>
                            <th class="my_text_center"> {{ $key + 1 }}</th>
                            <th class="my_text_center">{{ $order->code }}</th>
                            <th>{!! $order->productNames !!}</th>
                            <th class="my_text_center">{{ $order->tax }} %</th>
                            <th class="my_text_center">{{ $order->coupon }}</th>
                            <th class="my_text_center">{{ $order->user_paid }} $</th>
                        </tr>
                    @endforeach
                @endif
                </tbody>
            </table>
        </div>
    </div>

@endsection

@section('js')
    <script>
        $('#li_my_order').addClass('active');
    </script>

@endsection