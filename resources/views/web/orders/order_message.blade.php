@extends('web.layouts.master')
@section('content')
    <div class="row alert alert-info-custom">
        <div class="col-md-12 text-center">
            @include('web.layouts.components.alert')
            <h2>You pay: <strong>{{ $order->user_paid }} $</strong></h2>
            <br>
            <a href="{{ route('orders.index') }}" class="btn btn-primary">Go to My orders</a>
        </div>
    </div>

@endsection

@section('js')

@endsection