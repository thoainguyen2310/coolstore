@extends('web.layouts.master')
@section('content')
    <div class="row alert alert-info-custom">
        <div class="col-md-12 ">
            @include('web.layouts.components.alert')
            <div class="form-group">
                <h4>Product Detail</h4>
                <table class="table table-bordered table-data">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Name</th>
                        <th>Price</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(!empty($products))
                        @foreach($products as $key => $product)
                            <tr>
                                <th>{{ $key + 1 }}</th>
                                <th>{{ $product->name }}</th>
                                <th>{{ $product->price }} $</th>
                            </tr>
                        @endforeach
                    @endif
                    </tbody>
                </table>
            </div>

            <form method="post" action="{{ route('orders.purchase', ['code' => $order->code]) }}">
                {{ csrf_field() }}
                <table class="table table-bordered table-data">
                    <tbody>
                    <tr>
                        <td class="my_text_center">
                           <h4> Products total price</h4>
                        </td>
                        <td class="my_text_center">
                           <h3 class="text-green"> {{ $order->origin_cost }} $</h3>
                        </td>
                        <td class="my_text_center">
                            <h4> Tax</h4>
                        </td>
                        <td class="my_text_center">
                            <h3 class="text-green"> {{ $order->tax }} %</h3>
                        </td>
                    </tr>
                    </tbody>
                </table>
                <br>

                <div class="col-md-12">
                    <div class="col-md-4">
                        <label> If you input <strong>GO2018</strong>, the total order amount 10% less</label>
                    </div>
                    <div class="col-md-3">
                        <input class="col-md-3 form-control" name="coupon">
                    </div>
                </div>
                <br>
                <div class="col-md-12 text-center" style="margin-top: 1%">
                    <button type="submit" class="btn btn-success">Submit</button>
                </div>
            </form>
        </div>
    </div>

@endsection

@section('js')

@endsection