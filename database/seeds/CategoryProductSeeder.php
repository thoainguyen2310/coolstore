<?php

use Illuminate\Database\Seeder;

class CategoryProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $burger['name'] = 'Burgers';
        $burger['status'] = 1;
        $burger = \App\Models\Category::create($burger);

        $burgerProducts = [
            [
                'name' => 'Hotdog',
                'price' => 2,
                'currency' => 1
            ],
            [
                'name' => 'CheeseBurger',
                'price' => 3,
                'currency' => 1
            ],
            [
                'name' => 'Frie',
                'price' => 4,
                'currency' => 1
            ]
        ];

        foreach ($burgerProducts as $burgerProduct) {
            $product = new \App\Models\Product();
            $product->name = $burgerProduct['name'];
            $product->status = 1;
            $product->category_id = $burger->id;
            $product->code = \Illuminate\Support\Str::random(6);
            $product->price = $burgerProduct['price'];
            $product->currency = $burgerProduct['currency'];
            $product->save();
        }


        $beverages['name'] = 'Beverages';
        $beverages['status'] = 1;
        $beverages = \App\Models\Category::create($beverages);

        $beverageProducts = [
            [
                'name' => 'Coke',
                'price' => 2.5,
                'currency' => 1
            ],
            [
                'name' => 'Coffee',
                'price' => 3,
                'currency' => 1
            ],
            [
                'name' => 'Tea',
                'price' => 4.2,
                'currency' => 1
            ]
        ];

        foreach ($beverageProducts as $beverageProduct) {
            $product = new \App\Models\Product();
            $product->name = $beverageProduct['name'];
            $product->status = 1;
            $product->category_id = $beverages->id;
            $product->code = \Illuminate\Support\Str::random(6);
            $product->price = $beverageProduct['price'];
            $product->currency = $beverageProduct['currency'];
            $product->save();
        }

        $combo['name'] = 'Combo Meal';
        $combo['status'] = 1;
        $combo = \App\Models\Category::create($combo);

        $comboProducts = [
            [
                'name' => 'Chicken Combo Mea',
                'price' => 7,
                'currency' => 1
            ],
            [
                'name' => 'Pork Comb',
                'price' => 12,
                'currency' => 1
            ],
            [
                'name' => 'Fish Combo',
                'price' => 15,
                'currency' => 1
            ]
        ];

        foreach ($comboProducts as $comboProduct) {
            $product = new \App\Models\Product();
            $product->name = $comboProduct['name'];
            $product->status = 1;
            $product->category_id = $combo->id;
            $product->code = \Illuminate\Support\Str::random(6);
            $product->price = $comboProduct['price'];
            $product->currency = $comboProduct['currency'];
            $product->save();
        }


    }
}
