<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
//    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function index() {
        return view('web.auth.login')->with([
           'title' => 'Login',
           'subTitle' => ''
        ]);
    }

    public function login(Request $request) {
        $validator = Validator::make($request->all(),
            [
                'user_login' => ['required', 'string', 'max:255'],
                'password' => ['required', 'string', 'min:6', 'max:255'],
            ],
            [
                'user_login.*' => 'Phone or Email or Username Invalid',
                'password.*' => ' Password Invalid',
            ]);
        if($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }
        $userLogin = $request->input('user_login', null);
        $check = \App\Models\User::where('phone', $userLogin)
            ->orWhere('email', $userLogin)
            ->orWhere('username', $userLogin)->first();
        if($check) {
            if(Hash::check($request->input('password'), $check->password)) {
                $rememberLogin = $request->input('remember_login');
                if ($rememberLogin) {
                    Auth::guard('web')->login($check, true);
                } else {
                    Auth::guard('web')->login($check, false);
                }
                return redirect()->route('dashboard');
            } else {
                $request->session()->flash('error', 'Login is failed! Please try to again!');
                return redirect()->back()->withInput();
            }
        } else {
            $request->session()->flash('error', 'Register is failed! Please try to again!');
            return redirect()->back()->withInput();
        }

    }
}
