<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function index()
    {
        return view('web.auth.register')->with([
            'title' => 'Register',
            'subTitle' => ''
        ]);
    }

    public function create(Request $request)
    {
        $validator = Validator::make($request->all(),
            [
                'phone' => ['required', 'string', 'max:255'],
                'email' => ['required', 'email', 'max:255'],
                'username' => ['required', 'string', 'max:255'],
                'password' => ['required', 'string', 'min:6', 'max:255'],
                'confirm_password' => ['required', 'string','min:6', 'max:255'],
            ],
            [
                'phone.*' => 'Phone Invalid',
                'email.*' => 'Email Invalid',
                'username.*' => 'Username Invalid',
                'password.*' => 'Password Invalid',
                'confirm_password.*' => 'Confirm Password Invalid',
            ]);
        if($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $pass = $request->input('password');
        $confirmPass = $request->input('confirm_password');
        if(strcmp($pass, $confirmPass) != 0) {
            $request->session()->flash('error', 'Confirm Password is incorrect');
            return redirect()->back()->withInput();
        }
        $phone = $request->input('phone');
        $email = $request->input('email');
        $username = $request->input('username');
        $check = \App\Models\User::where('phone', $phone)
            ->orWhere('email', $email)
            ->orWhere('username', $username)->first();
        if($check) {
            $request->session()->flash('error', 'Account is ready exist!');
            return redirect()->back()->withInput();
        }
        $userData['username'] = $username;
        $userData['email'] = $email;
        $userData['phone'] = $phone;
        $userData['fullname'] = $request->input('fullname', null);
        $userData['password'] = Hash::make($pass);

        $newUser = \App\Models\User::insert($userData);
        if($newUser) {
            $request->session()->flash('success', 'Register is successful! Please Login!');
            return redirect()->route('login');
        } else {
            $request->session()->flash('error', 'Register is failed! Please try to again!');
            return redirect()->back()->withInput();
        }

    }


}
