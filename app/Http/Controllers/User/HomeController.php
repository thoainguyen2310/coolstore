<?php

namespace App\Http\Controllers\User;

use App\Repository\Repositories\CategoryRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HomeController extends Controller
{
    private $categoryRepo;


    public function __construct()
    {
        $this->categoryRepo = new CategoryRepository();
    }

    public function index()
    {
        $categories = $this->categoryRepo->all();
        $categories = $this->categoryRepo->formatAllRecord($categories);
        return view('web.home.dashboard')->with([
            'title' => 'Menu',
            'subTitle' => '',
            'categories' => $categories
        ]);
    }


}
