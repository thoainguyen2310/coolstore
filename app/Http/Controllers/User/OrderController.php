<?php


namespace App\Http\Controllers\User;


use App\Http\Controllers\Controller;
use App\Models\OrderProduct;
use App\Models\Product;
use App\Repository\Repositories\OrderRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class OrderController extends Controller
{
    private $orderRepository;

    public function __construct()
    {
        $this->orderRepository = new OrderRepository();
    }

    public function index(Request $request) {
        $user = Auth::guard('web')->user();
        $orders = $this->orderRepository->all(['user_id' => $user->id], ['by' => 'created_at', 'type' => 'desc']);
        $orders = $this->orderRepository->formatAllRecord($orders);
        return view('web.orders.index')->with([
           'title' => 'My orders',
           'sunTitle' => '',
           'orders' => $orders
        ]);
    }

    public function create(Request $request)
    {
        $user = Auth::guard('web')->user();
        $productIds = $request->input('select_product', null);
        if (empty($productIds)) {
            $request->session()->flash('error', 'Please Choose Item!');
            return redirect()->back()->withInput();
        }
        try {
            DB::beginTransaction();

            $products = Product::whereIn('id', $productIds)->get();
            $originCost = Product::whereIn('id', $productIds)->sum('price');

            $orderData = [];
            $orderData['status'] = config('apps.order.status_str.pending');
            $orderData['code'] = Str::random(8);
            $orderData['user_id'] = $user->id;
            $orderData['origin_cost'] = $originCost;
            $orderData['tax'] = config('apps.order.tax_default');
            $orderData['user_paid'] = $originCost + $originCost * $orderData['tax'] / 100;

            $newOrder = $this->orderRepository->store($orderData);
            if($newOrder) {
                foreach ($productIds as $productId) {
                    $orderProduct = new OrderProduct();
                    $orderProduct->order_id = $newOrder->id;
                    $orderProduct->product_id = $productId;
                    if(!$orderProduct->save()) {
                        throw new \Exception('Error!');
                    }
                }
            }
            DB::commit();
            return redirect()->route('orders.view', ['code' => $newOrder->code]);
        } catch (\Exception $exception) {
            $request->session()->flash('error', 'Error! Please try again!'.$exception->getMessage());
            return redirect()->back()->withInput();
        }

    }

    public function orderView(Request $request, $code)
    {
        $order = $this->orderRepository->getByCode($code);
        if(!$order) {
            $request->session()->flash('error', 'The order do not exist!');
            return redirect()->route('dashboard');
        }

        $productIds = OrderProduct::where('order_id', $order->id)->pluck('product_id');

        $products = Product::whereIn('id', $productIds)->get();

        return view('web.orders.order_view')->with([
            'title' => 'Order',
            'products' => $products,
            'order' => $order
        ]);
    }

    public function purchase(Request $request, $code) {
        $order = $this->orderRepository->getByCode($code);
        $order->status = config('apps.order.status_str.paid_success');
        $coupon = $request->input('coupon', null);
        if(!$coupon) {
            $order->save();
            $request->session()->flash('success', 'Order success!');
            return redirect()->route('orders.message', ['code' => $code]);
        }
        if(strcmp($coupon, 'GO2018') != 0) {
            $request->session()->flash('error', 'Coupon is invalid');
            return redirect()->back()->withInput();
        }
        $order->user_paid = $order->user_paid - $order->user_paid * 10 / 100;
        $order->coupon = $coupon;
        if($order->save()) {
            $request->session()->flash('success', 'Order success!');
            return redirect()->route('orders.message', ['code' => $code]);
        } else {
            $request->session()->flash('error', 'Error');
            return redirect()->back()->withInput();
        }
    }

    public function orderMessage(Request $request, $code) {
        $order = $this->orderRepository->getByCode($code);
        return view('web.orders.order_message')->with([
            'title' => 'Order Success',
            'order' => $order
        ]);
    }


}