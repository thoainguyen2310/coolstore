<?php


namespace App\Repository\Repositories;


use App\Repositories\Repository;

class ProductRepository extends Repository
{
    public function __construct()
    {
        parent::__construct('App\Models\Product');
        $this->fields = ['category_id', 'status', 'code', 'name', 'price', 'currency',
            'created_at', 'updated_at', 'deleted_at'];

    }

    public function formatAllRecord($records)
    {
        if (count($records) > 0) {
            foreach ($records as $record) {
                $record = $this->formatRecord($record);
            }
        }
        return $records;
    }

    public function formatRecord($record)
    {

        return $record;
    }
}