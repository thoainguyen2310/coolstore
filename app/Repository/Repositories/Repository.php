<?php
namespace App\Repositories;

class Repository {

    private $model;
    protected $fields;

    public function __construct($model) {
        $this->model = $model;
        $this->fields = [];
    }

    public function store($data) {
        if (empty($this->fields) || empty($data)) {
            return false;
        }

        $object = new $this->model;
        foreach ($this->fields as $field) {
            if (array_key_exists($field, $data)) {
                $object->$field = $data[$field];
            }
        }
        if ($object->save()) {
            return $object;
        }
        return false;
    }

    public function update($object, $data) {
        if (empty($this->fields) || empty($data)) {
            return false;
        }

        foreach ($this->fields as $field) {
            if (array_key_exists($field, $data)) {
                $object->$field = $data[$field];
            }
        }
        if ($object->save()) {
            return $object;
        }
        return false;
    }

    public function get($filters, $take = null, $sort = []) {
        $data = new $this->model;

        if (!$take) {
            $take = @$_COOKIE["per_page"] ? @intval($_COOKIE["per_page"]) : 10;
        }

        if (!empty($filters)) {
            foreach ($filters as $key => $value) {
                if ($value == "NOT_FILTER") {
                    continue;
                }
                if (in_array($key, $this->fields)) {
                    if ($value == "NULL") {
                        $data = $data->whereNull($key);
                    } elseif ($value == "NOT_NULL") {
                        $data = $data->whereNotNull($key);
                    } else {
                        $data = $data->where($key, $value);
                    }
                }
            }
        }

        if (is_array($sort) && !empty($sort['by']) && !empty($sort['type'])) {
            $data = $data->orderBy($sort['by'], $sort['type']);
        } else {
            $data = $data->orderBy('id', 'desc');
        }
        return $data->paginate($take);
    }

    public function all($filters = [], $sort = []) {
        $data = new $this->model;

        if (!empty($filters)) {
            foreach ($filters as $key => $value) {
                if (in_array($key, $this->fields)) {
                    if (strcmp($value, "NULL") == 0) {
                        $data = $data->whereNull($key);
                    } elseif (strcmp($value, "NOT_NULL") == 0) {
                        $data = $data->whereNotNull($key);
                    } else {
                        $data = $data->where($key, $value);
                    }
                }
            }
        }

        if (is_array($sort) && !empty($sort['by']) && !empty($sort['type'])) {
            $data = $data->orderBy($sort['by'], $sort['type']);
        }
        
        return $data->get();
    }

    public function count($filters) {
        $data = new $this->model;

        if (!empty($filters)) {
            foreach ($filters as $key => $value) {
                if (in_array($key, $this->fields)) {
                    $data = $data->where($key, $value);
                }
            }
        }

        return $data->count();
    }

    public function sum($filters, $column) {
        $data = new $this->model;

        if (!empty($filters)) {
            foreach ($filters as $key => $value) {
                if (in_array($key, $this->fields)) {
                    $data = $data->where($key, $value);
                }
            }
        }

        return $data->sum($column);
    }

    public function getById($id) {
        if (!$id) {
            return false;
        }
        return $this->model::find($id);
    }

    // Thêm phương thức getBySlug
    public function getBySlug($slug, $filters = []){
        if(!$slug){
            return false;
        }
        if(empty($filters)){
            return $this->model::where('slug',$slug)->first();
        }
        else{
            $filters['slug'] = $slug;
            return $this->model::where($filters)->first();
        }
    }
}
