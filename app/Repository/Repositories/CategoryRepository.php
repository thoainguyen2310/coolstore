<?php


namespace App\Repository\Repositories;


use App\Repositories\Repository;

class CategoryRepository extends Repository
{
    public function __construct()
    {
        parent::__construct('App\Models\Category');
        $this->fields = ['status', 'name', 'created_at', 'updated_at', 'deleted_at'];

    }

    public function formatAllRecord($records)
    {
        if (count($records) > 0) {
            foreach ($records as $record) {
                $record = $this->formatRecord($record);
            }
        }
        return $records;
    }

    public function formatRecord($record)
    {
        $record->products;
        return $record;
    }
}