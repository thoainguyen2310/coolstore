<?php


namespace App\Repository\Repositories;


use App\Models\Order;
use App\Models\OrderProduct;
use App\Models\Product;
use App\Repositories\Repository;
use Carbon\Carbon;

class OrderRepository extends Repository
{
    public function __construct()
    {
        parent::__construct('App\Models\Order');
        $this->fields = ['status', 'code', 'user_id', 'origin_cost', 'user_paid', 'coupon', 'tax',
            'created_at', 'updated_at', 'deleted_at'];

    }

    public function getByCode($code) {
        return Order::where('code', $code)->first();
    }

    public function formatAllRecord($records)
    {
        if (count($records) > 0) {
            foreach ($records as $record) {
                $record = $this->formatRecord($record);
            }
        }
        return $records;
    }

    public function formatRecord($record)
    {
        $record->createAt = Carbon::parse($record->created_at)->format('d-m-Y');
        $productIds = OrderProduct::where('order_id', $record->id)->pluck('product_id');

        $products = Product::whereIn('id', $productIds)->get();
        $productNames = '';
        foreach ($products as $product) {
            $productNames .= "<li> $product->name : $product->price $ </li>";
        }
        $record->productNames = $productNames;
        return $record;
    }
}