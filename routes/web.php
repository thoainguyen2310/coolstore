<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'User\HomeController@index')->name('dashboard');

Route::get('/login', 'Auth\LoginController@index')->name('login');
Route::post('/login', 'Auth\LoginController@login')->name('login');

Route::get('/register', 'Auth\RegisterController@index')->name('register');
Route::post('/register', 'Auth\RegisterController@create')->name('register');

Route::group(['middleware' => 'auth'], function () {
    Route::get('/logout', 'Auth\LoginController@logout')->name('logout');

    Route::post('/create', 'User\OrderController@create')->name('orders.create');
    Route::get('/orderView/{code}', 'User\OrderController@orderView')->name('orders.view');
    Route::post('/orderView/{code}', 'User\OrderController@purchase')->name('orders.purchase');
    Route::get('/orderMessage/{code}', 'User\OrderController@orderMessage')->name('orders.message');

    Route::get('/orders', 'User\OrderController@index')->name('orders.index');

});